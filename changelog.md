0.1.0

* Rework data types and functions

0.0.2

* Widen dependency version boundaries
* Generalise some function signatures
* Introduce some functions

0.0.1

* This change log starts
